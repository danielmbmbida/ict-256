$(function () {
    $.validator.addMethod("PWCHECK",
        function (value,element) {
             if( "/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/".test(value)){
                 return true;
             }else{
                 false;
             }

        })
});
$(function () {
    $("#inscription_form"). validate({
        rules:{
            nom_per:{
              required:true,
              minlength:2,
            },
            prenom_per:{
            required: true,
            minlength: 2,
            },
            email_per:{
                required:true,
                email: true,
            },
            password_per:{
                required:true,

            },
            password_conf: {
                required: true,
                equalTo: "#password_per"
            }
        },
        messages:{
            nom_per:{
               required:"Veuiller saisir votre nom",
                minlength:"Votre nom doir être composé de2 caracatères au minimum"
            },
            prenom_per:{
                required:"Veuiller saisir votre nom",
                minlength:"Votre nom doir être composé de2 caracatères au minimum"
            },
            email_per:{
                required:"Veuillez saisir votre E-mail",
                email:"Votre adresse e-mail doit avoir le format suivant: name@domain.com"
            },
            password_per:{
                required:"Veuillez saisir votre mot de passe",
                PWCHECK:"Le mot de passe doit contenir au minimum 8 caractères, dont une minuscle, une majuscule, un chiffre et caractère spécial."
            },
            password_conf:{
                required:"Veuillez saisir votre mot de passe",
                equalTo:"Les mots de passe ne sont pas identiques"

            }
        },

    });
});
