$(function () {

    $("#lbl_nbr_eleve").hide();
    $("#nbr_eleve").hide();
    $("#nom_club").hide();

    $("#moniteur_oui").click(function () {
        if($("#moniteur_oui").is(":checked")){ //à la place de $("#moniteur_oui") on peut mettre $(this)
            $("#lbl_nbr_eleve").show();
            $("#nbr_eleve").show();
            $("#nbr_eleve").validate({
                rules:{
                    required:true
                },
                message:{
                    required: "Veuillez entrer le nombre d'élèves"
                }
            });
        }
        else {
            $("#lbl_nbr_eleve").hide();
            $("#nbr_eleve").hide();
        }
    });

    $("[name=type_club]").change(function () {
        //console.log($("[name=type_club]:checked").val());
        if($("[name=type_club]:checked").val()==1){
            $("#nom_club").show();
        }
        else {
            $("#nom_club").hide();
        }

    });

});
