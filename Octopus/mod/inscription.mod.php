<div id="add_ins_mod" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!--Modal Content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3>Fiche d'inscription</h3>
            </div>
            <div class="modal-body">

                <form method="post" id="inscription_form">

                    <div  class="form-group row">
                        <label class="col-md-3 col-form-label" ><!--col-md-2-->
                            Moniteur ?
                        </label>
                        <div class="col-md-2">
                            <input type="checkbox" id="moniteur_oui" class="form-input " name="moniteur_oui">

                        </div>
                        <div class="col-md-7">
                            <label id="lbl_nbr_eleve" class="col-md-8">Nombre d'élèves</label>
                            <input type="text" class="col-md-2" id="nbr_eleve" name="nbr_eleve">
                        </div>
                    </div>

                    <!--<div class="form-group row">
                        <div class="col-sm-offset-2 checkbox">
                            <label class="col-sm-10 col-form-label">
                                <input type="checkbox" value="1" checked id="news_letter" name="news_letter"> La formation d'informaticien m'intéresse
                            </label>
                        </div>
                    </div>-->

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" ><!--col-md-2-->
                            Catégorie
                        </label>

                        <div class="col-md-offset-3 ">
                            <label class="col-md-4 col-form-label">
                                <input type="radio" id="categorie" name="categorie"> Plongeur
                            </label>

                            <label class="col-md-4 col-form-label">
                                <input type="radio" id="categorie" name="categorie"> Nageur
                            </label>

                            <label class="col-md-4 col-form-label">
                                <input type="radio" id="categorie" name="categorie"> Apnéiste
                            </label>

                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="nom_per" class="col-md-3 col-form-label">
                            Nom
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="nom_per" name="nom_per" placeholder="Votre nom">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="prenom_per" class="col-md-3 col-form-label">
                            Prénom
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="prenom_per" name="prenom_per" placeholder="Votre prénom">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="tel" class="col-md-3 col-form-label">
                            Téléphone
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="tel" name="tel" placeholder="Votre numéro de téléphone">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="email" class="col-md-3 col-form-label">
                            E-Mail
                        </label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Votre adresse e-mail">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" ><!--col-md-2-->
                            Catégorie
                        </label>

                        <div class="col-md-offset-3 ">
                            <label class="col-md-6 col-form-label">
                                <input type="radio"value="1" class="type" name="type_club"> Club / Ecole
                                <input type="text" class="form-control" id="nom_club" name="nom_club" placeholder="Nom du Club / Ecole">
                            </label>

                            <label class="col-md-10 col-form-label">
                                <input type="radio" value="0" class="type" name="type"> Privé
                            </label>
                        </div>
                    </div>

                </form>


                <div class="modal-footer">
                    <div class="col-md-offset-6 col-md-3">
                        <input type="submit" class="form-control btn btn-primary submit" id="submit_conf" value="Confirmer">
                    </div>

                    <div class="col-md-3">
                        <input type="reset" class="form-control btn btn-warning" data-dismiss="modal" id="reset_conf" value="Annuler">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="./regles.js"></script>
<script>
    $("#add_ins_mod").modal("show");
</script>
